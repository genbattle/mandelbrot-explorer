# Mandelbrot Explorer #

![An example of the Mandelbrot Set](mandelbrot.png)

This application is a simple interactive [Mandelbrot Set](https://en.wikipedia.org/wiki/Mandelbrot_set) fractal generator. It was created mainly as a technical experiment to practice using [OpenCL](https://www.khronos.org/opencl/), [CMake](http://www.cmake.org/), [SFML](http://www.sfml-dev.org/), and [NanoSignalSlot](https://github.com/NoAvailableAlias/nano-signal-slot).

## Dependencies ##

- OpenCL (1.1+)
- SFML (2.3+)
- CMake (3.0+)
- GCC (5.0+ recommended) or Clang (3.4+)
 
 Currently only tested on Arch Linux.

## Building ##

1. [Clone](https://bitbucket.org/genbattle/mandelbrot-explorer/src) or [download](https://bitbucket.org/genbattle/mandelbrot-explorer/downloads) the code from BitBucket.
2. Create a `build` directory and `cd` into it inside a terminal or command window.
3. Run `cmake ..`.
4. Run `make`.

## Running ##

To run the application execute the `mandelbrot` binary.

Once the application is running you can use a mouse scroll wheel or touchpad scroll gesture to zoom in or out of the Mandelbrot render. When you zoom, the next frame will be rendered. With the default (naive) renderer, this may take a large amount of time.

- To switch backends, press the `b` key. The available backends are:
  - The single-threaded naive CPU renderer (the default).
  - The OpenCL renderer, which will try and select a compatible GPU device to render on.
- To close the application either use the close button on the window, or press the `q` or `esc` keys.