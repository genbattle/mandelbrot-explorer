cmake_minimum_required(VERSION 3.0)

project(mandelbrot)

set(DEBUG_FLAGS "-std=c++14 -g -O0 -Wall -Wextra -Werror -Wpedantic")
set(RELEASE_FLAGS "-std=c++14 -O3 -Wall -Wextra -Werror -Wpedantic")

set(CMAKE_CXX_FLAGS ${RELEASE_FLAGS})
set(CMAKE_CXX_FLAGS_DEBUG ${DEBUG_FLAGS})
set(CMAKE_CONFIGURATION_TYPES Debug Release)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

add_subdirectory(src)