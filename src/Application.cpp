#include "Application.hpp"
#include "impl/Naive.hpp"
#include "impl/OpenCL.hpp"

#include <cstdint>
#include <iostream>

namespace {
// TODO: clean up these expressions to be variably configurable
constexpr int WINDOW_WIDTH = 1024;
constexpr int WINDOW_HEIGHT = 768;
constexpr int TEXTURE_BPP = 4;
constexpr float MANDELBROT_LIMIT = 2.0f;

const mandelbrot::Palette palette_default = {{120, 0, 60},
                                             {140, 30, 100},
                                             {180, 80, 120},
                                             {220, 120, 150},
                                             {150, 120, 220},
                                             {120, 80, 180},
                                             {100, 30, 140},
                                             {60, 0, 120}};
}

namespace mandelbrot {

Application::Application() : m_implementation_type(Backend::Naive) {
	// Set up window, frame buffers, etc.

	m_window =
	    std::make_unique<sf::RenderWindow>(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Mandelbrot",
	                                       sf::Style::Titlebar | sf::Style::Close);
	m_window->setVerticalSyncEnabled(true);
	// TODO: Move buffer ownership into the implementation?
	m_raw_buffer = std::make_unique<uint8_t[]>(WINDOW_WIDTH * WINDOW_HEIGHT * TEXTURE_BPP);
	m_buffer =
	    std::make_unique<Buffer>(m_raw_buffer.get(), WINDOW_WIDTH, WINDOW_HEIGHT, TEXTURE_BPP);
	m_viewport = std::make_unique<Viewport>(-2.0f, 2.0f, 4.0f, 4.0f, 1000);

	m_texture = std::make_unique<sf::Texture>();
	m_texture->create(WINDOW_WIDTH, WINDOW_HEIGHT);
	m_sprite = std::make_unique<sf::Sprite>();
	m_sprite->setTexture(*m_texture);
	m_eventrouter = std::make_unique<EventRouter>(*m_window);
	m_implementation = std::make_unique<impl::Naive>();
}

void Application::main_loop() {
	connect_events();
	while (m_window->isOpen()) {
		m_window->clear(sf::Color::Blue);
		// Update texture
		m_implementation->Mandelbrot(*m_buffer, *m_viewport, palette_default);
		// Draw texture to screen
		m_texture->update(static_cast<unsigned char*>(m_raw_buffer.get()));
		m_window->draw(*m_sprite);
		m_window->display();
		// Process events
		m_eventrouter->Poll();
	}
}

void Application::connect_events() {
	// Link up events to the event router
	m_eventrouter->m_mouse_wheel_event.connect<Application, &Application::handle_scroll_zoom>(this);
	m_eventrouter->m_resize_event.connect<Application, &Application::handle_resize_window>(this);
	m_eventrouter->m_key_released_event.connect<Application, &Application::handle_key_close>(this);
	m_eventrouter->m_key_released_event.connect<Application, &Application::handle_key_backend>(
	    this);
	m_eventrouter->m_close_event.connect<Application, &Application::handle_window_close>(this);
}

void Application::handle_resize_window(const sf::Event& event) {
	m_raw_buffer = std::make_unique<uint8_t[]>(event.size.width * event.size.height * TEXTURE_BPP);
	m_buffer = std::make_unique<Buffer>(m_raw_buffer.get(), event.size.width, event.size.height,
	                                    TEXTURE_BPP);
	m_texture = std::make_unique<sf::Texture>();
	m_texture->create(event.size.width, event.size.height);
	m_sprite->setTexture(*m_texture);
}

void Application::handle_window_close(const sf::Event& /*event*/) {
	m_window->close();
}

void Application::handle_scroll_zoom(const sf::Event& event) {
	if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel) {
		sf::Vector2u window_size = m_window->getSize();
		sf::Vector2f mousepct(
		    static_cast<float>(event.mouseWheelScroll.x) / static_cast<float>(window_size.x),
		    static_cast<float>(event.mouseWheelScroll.y) / static_cast<float>(window_size.y));
		sf::Vector2f viewpos(m_viewport->left + m_viewport->width * mousepct.x,
		                     m_viewport->top - m_viewport->height * mousepct.y);
		// Increase or decrease width by 20% based on mouse wheel movement
		if (event.mouseWheelScroll.delta < 0) {
			m_viewport->width *= 1.2;
			m_viewport->height *= 1.2;
		} else {
			m_viewport->width *= 0.8;
			m_viewport->height *= 0.8;
		}
		// Impose lower limits on zoom
		if (m_viewport->width > 4.0 || m_viewport->height > 4.0) {
			m_viewport->left = -2.0;
			m_viewport->top = 2.0;
			m_viewport->width = 4.0;
			m_viewport->height = 4.0;
		} else {
			m_viewport->left = viewpos.x - mousepct.x * m_viewport->width;
			m_viewport->top = viewpos.y + mousepct.y * m_viewport->height;
		}
	}
}

void Application::handle_key_close(const sf::Event& event) {
	switch (event.key.code) {
	case sf::Keyboard::Q:
	case sf::Keyboard::Escape:
		m_window->close();
		break;
	default:
		break;
	}
	if (event.key.code == sf::Keyboard::Q) {
	}
}

void Application::handle_key_backend(const sf::Event& event) {
	// TODO: implement backend switching
	if (event.key.code == sf::Keyboard::B) {
		switch (m_implementation_type) {
		case Backend::Naive:
			m_implementation_type = Backend::OpenCL;
			m_implementation = std::make_unique<impl::OpenCL>();
			break;
		case Backend::OpenCL:
			m_implementation_type = Backend::Naive;
			m_implementation = std::make_unique<impl::Naive>();
			break;
		}
	}
}
}