#pragma once
#ifndef SRC_PALETTE_HPP_
#define SRC_PALETTE_HPP_

#include <vector>
#include <cstdint>

namespace mandelbrot {

struct Color {
	uint8_t r, g, b;
};

typedef std::vector<Color> Palette;

} /* namespace mandelbrot */

#endif /* SRC_PALETTE_HPP_ */
