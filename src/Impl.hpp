#pragma once
#ifndef SRC_IMPL_HPP_
#define SRC_IMPL_HPP_
#include "Palette.hpp"

namespace mandelbrot {
struct Buffer;
struct Viewport;

class Impl {
public:
	virtual ~Impl(){};
	/*
	 * Generate a mandelbrot pattern, depositing the result in the memory pointed
	 * to by buffer, which
	 * should be (width * height * 3) bytes long.
	 */
	virtual void Mandelbrot(Buffer& buffer, const Viewport& view, const Palette& palette) = 0;

protected:
	static constexpr float escape_limit = 2.0;
};

} /* namespace mandelbrot */

#endif /* SRC_IMPL_HPP_ */
