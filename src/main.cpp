#include "Application.hpp"
#include "Palette.hpp"

#include <cstdint>
#include <type_traits>
#include <iostream>

int main() {
	// This statically checks an assumption made about the relative sizes of
	// unsigned char and
	// uint8_t
	static_assert(std::is_same<unsigned char, uint8_t>::value,
	              "uint8_t is not equivalent to unsigned char");

	// TODO: Move current algorithms into an "escape" namespace, create continuous
	// shading examples

	mandelbrot::Application mandelbrot;
	mandelbrot.main_loop();

	return 0;
}
