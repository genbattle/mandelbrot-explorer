#pragma once
#ifndef SRC_EVENTROUTER_HPP_
#define SRC_EVENTROUTER_HPP_

#include <nano_signal_slot.hpp>

namespace sf {
class Event;
class Window;
}

namespace mandelbrot {
/*
 * The EventRouter class centralizes event polling and allows other objects to
 *subscribe only for
 * the specific events they want to receive.
 *
 * TODO: When the number of events handled here grows sufficiently large, store
 *signals in a map
 * indexed by the event type.
 */
class EventRouter {
public:
	EventRouter(sf::Window& window);

	// Make object non-copyable or movable - doesn't make sense
	EventRouter(const EventRouter&) = delete;
	EventRouter(EventRouter&&) = delete;
	EventRouter& operator=(const EventRouter&) = delete;
	EventRouter& operator=(EventRouter&&) = delete;

	// Poll the window for events and send the appropriate signals in response
	void Poll();

	// Event signals that external objects can connect to for events
	Nano::Signal<void(const sf::Event&)> m_mouse_wheel_event;
	Nano::Signal<void(const sf::Event&)> m_resize_event;
	Nano::Signal<void(const sf::Event&)> m_key_released_event;
	Nano::Signal<void(const sf::Event&)> m_close_event;

private:
	sf::Window& m_window;
};

} /* namespace mandelbrot */

#endif /* SRC_EVENTROUTER_HPP_ */
