#include "Naive.hpp"
#include "Buffer.hpp"
#include "Viewport.hpp"

#include <cassert>
#include <cstddef>

namespace mandelbrot {
namespace impl {

void Naive::Mandelbrot(Buffer& buffer, const Viewport& view, const Palette& palette) {
	assert(buffer.bpp == 4); // only 4 BPP supported currently
	// find the center of the buffer
	constexpr float threshold = 4; // square of escape limit

	// TODO: Split individual pixel calculation and color palette into separate functions
	// TODO: Optimize
	for (int y = 0; y < buffer.height; ++y) {
		for (int x = 0; x < buffer.width; ++x) {
			// virtual positions where (0,0) is the image center and (2,2) (-2,-2) are the bounds
			const float virt_x =
			    view.left + view.width * (static_cast<float>(x) / static_cast<float>(buffer.width));
			const float virt_y =
			    view.top -
			    view.height * (static_cast<float>(y) / static_cast<float>(buffer.height));
			float z_x = virt_x;
			float z_y = virt_y;
			float temp = 0.0f;
			uint32_t count = 1;
			while (z_x * z_x + z_y * z_y < threshold && count != view.limit) {
				temp = z_x * z_x - z_y * z_y + virt_x;
				z_y = 2.0f * z_x * z_y + virt_y;
				z_x = temp;
				++count;
			}

			size_t index = y * buffer.width * buffer.bpp + x * buffer.bpp;
			// TODO: Improve colour range
			if (count != view.limit) {
				auto pidx = count % palette.size();
				buffer.data[index + 0] = palette[pidx].r;
				buffer.data[index + 1] = palette[pidx].g;
				buffer.data[index + 2] = palette[pidx].b;
			} else {
				buffer.data[index + 0] = 0;
				buffer.data[index + 1] = 0;
				buffer.data[index + 2] = 0;
				buffer.data[index + 3] = 255;
			}
			buffer.data[index + 3] = 255;
		}
	}
}

} /* namespace impl */
} /* namespace mandelbrot */
