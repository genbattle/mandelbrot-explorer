#include "OpenCL.hpp"
#include "Buffer.hpp"
#include "Viewport.hpp"

#include <vector>
#include <cstdlib>
#include <cstdio>
#include <exception>
#include <iostream>

namespace mandelbrot {
namespace impl {

// TODO: Use OpenCL Image support
// TODO: pass escape limit as a parameter as in Naive example
namespace {
const char program_source[] = "\
		struct Viewport {\
			float left, top, width, height;\
			uint limit;\
		};\
		\
		struct Color {\
			uchar r, g, b;\
		};\
		\
		__kernel void mandelbrot(__global read_only struct Viewport* view,\
				__global read_only struct Color* palette,\
				__private uint palette_count,\
				__global write_only unsigned char* buffer,\
				__private int bpp) {\
			int x = get_global_id(0);\
			int y = get_global_id(1);\
			int width = get_global_size(0);\
			int height = get_global_size(1);\
			float virt_x = view->left + view->width * (convert_float(x) / convert_float(width));\
			float virt_y = view->top - view->height * (convert_float(y) / convert_float(height));\
			float z_x = virt_x;\
			float z_y = virt_y;\
			float temp = 0.0f;\
			uint count = 1;\
			while (z_x * z_x + z_y * z_y < 4.0f && count != view->limit) {\
				temp = z_x * z_x - z_y * z_y + virt_x;\
				z_y = 2.0f * z_x * z_y + virt_y;\
				z_x = temp;\
				++count;\
			}\
			\
			int index = y * width * bpp + x * bpp;\
			if (count != view->limit) {\
				uint pal_index = count % palette_count;\
				buffer[index + 0] = palette[pal_index].r;\
				buffer[index + 1] = palette[pal_index].g;\
				buffer[index + 2] = palette[pal_index].b;\
				buffer[index + 3] = 255;\
			} else {\
				buffer[index + 0] = 0;\
				buffer[index + 1] = 0;\
				buffer[index + 2] = 0;\
				buffer[index + 3] = 255;\
			}\
		}";
}

OpenCL::OpenCL() : m_err(CL_SUCCESS) {
	// search for GPU devices
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	bool found_device = false;
	for (auto& p : platforms) {
		// Find platform with GPU devices
		cl_context_properties properties[]{CL_CONTEXT_PLATFORM, (cl_context_properties)(p)(), 0};
		m_context = cl::Context(CL_DEVICE_TYPE_GPU, properties);
		std::vector<cl::Device> devices = m_context.getInfo<CL_CONTEXT_DEVICES>();
		if (devices.size() > 0) {
			// found a context with GPU devices, just use the first device
			m_device = cl::Device(devices[0]);
			found_device = true;
			break;
		}
	}
	// Found a GPU device, compile program
	if (found_device) {
		cl::Program::Sources source{std::make_pair(program_source, strlen(program_source))};
		cl::Program program{m_context, source};
		m_err = program.build(std::vector<cl::Device>(1, m_device));
		if (m_err != CL_SUCCESS) {
			std::string err_string;
			program.getBuildInfo(m_device, CL_PROGRAM_BUILD_LOG, &err_string);
			std::cout << "<---- CL Build Info ---->\n" << err_string << std::endl;
			throw std::runtime_error("Failed to compile OpenCL program");
		}
		m_kernel = {program, "mandelbrot", &m_err};
		if (m_err != CL_SUCCESS) {
			throw std::runtime_error("Failed to create OpenCL kernel");
		}
		m_queue = {m_context, m_device, 0, &m_err};
		if (m_err != CL_SUCCESS) {
			throw std::runtime_error("Failed to create OpenCL command queue");
		}
	} else {
		throw std::runtime_error("Unable to find a GPU device");
	}
}

void OpenCL::Mandelbrot(Buffer& buffer, const Viewport& view, const Palette& palette) {
	size_t bufsize = sizeof(uint8_t) * buffer.width * buffer.height * buffer.bpp;
	size_t palsize = sizeof(Palette::size_type) * palette.size();
	// Create OpenCL buffers
	cl::Buffer cl_vp = {m_context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, sizeof(Viewport),
	                    nullptr, nullptr};
	cl::Buffer cl_pal = {m_context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, palsize, nullptr,
	                     nullptr};
	cl::Buffer cl_buf = {m_context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, bufsize, nullptr,
	                     nullptr};
	// Copy data to GPU memory // TODO: pick up error values
	m_queue.enqueueWriteBuffer(cl_vp, CL_TRUE, 0, sizeof(Viewport), &view, nullptr, nullptr);
	m_queue.enqueueWriteBuffer(cl_pal, CL_TRUE, 0, palsize, palette.data(), nullptr, nullptr);

	m_kernel.setArg(0, cl_vp);
	m_kernel.setArg(1, cl_pal);
	m_kernel.setArg(2, static_cast<uint32_t>(palette.size()));
	m_kernel.setArg(3, cl_buf);
	m_kernel.setArg(4, buffer.bpp);

	cl::Event event;
	m_queue.enqueueNDRangeKernel(m_kernel, cl::NullRange, cl::NDRange(buffer.width, buffer.height),
	                             cl::NullRange, nullptr, &event);
	event.wait();

	m_queue.enqueueReadBuffer(cl_buf, CL_TRUE, 0, bufsize, buffer.data, nullptr, nullptr);
	m_queue.finish();
}

} /* namespace impl */
} /* namespace mandelbrot */
