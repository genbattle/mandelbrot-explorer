#pragma once
#ifndef SRC_IMPL_NAIVE_HPP_
#define SRC_IMPL_NAIVE_HPP_

#include "Impl.hpp"
#include "Palette.hpp"

namespace mandelbrot {
namespace impl {

class Naive : public ::mandelbrot::Impl {
public:
	void Mandelbrot(mandelbrot::Buffer& buffer, const mandelbrot::Viewport& view,
	                const mandelbrot::Palette& palette) override final;
};

} /* namespace impl */
} /* namespace mandelbrot */

#endif /* SRC_IMPL_NAIVE_HPP_ */
