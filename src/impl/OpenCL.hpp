#pragma once
#ifndef SRC_IMPL_OPENCL_HPP_
#define SRC_IMPL_OPENCL_HPP_

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif
#include "Impl.hpp"
#include "Palette.hpp"
#include <memory>

namespace mandelbrot {
namespace impl {

class OpenCL : public ::mandelbrot::Impl {
public:
	OpenCL();
	~OpenCL() override final{};

	void Mandelbrot(mandelbrot::Buffer& buffer, const mandelbrot::Viewport& view,
	                const mandelbrot::Palette& palette) override final;

private:
	cl::Context m_context;
	cl::Device m_device;
	cl::Kernel m_kernel;
	cl::CommandQueue m_queue;
	cl_int m_err;
};

} /* namespace impl */
} /* namespace mandelbrot */

#endif /* SRC_IMPL_OPENCL_HPP_ */
