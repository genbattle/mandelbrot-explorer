#pragma once
#ifndef SRC_APPLICATION_HPP_
#define SRC_APPLICATION_HPP_
/*
The Application class encompasses the top-level state and event handling.
*/
#include "Buffer.hpp"
#include "Viewport.hpp"
#include "EventRouter.hpp"
#include "impl/Naive.hpp"

#include <SFML/Graphics.hpp>
#include <nano_observer.hpp>
#include <memory>

namespace sf {
class Event;
}

namespace mandelbrot {
class Impl;

class Application : public Nano::Observer {
public:
	Application();

	void main_loop();
	void handle_resize_window(const sf::Event& event);
	void handle_window_close(const sf::Event& event);
	void handle_scroll_zoom(const sf::Event& event);
	void handle_key_close(const sf::Event& event);
	void handle_key_backend(const sf::Event& event);
	// TODO
	// void handle_window_resize(sf::Event event);

private:
	enum class Backend { Naive, OpenCL } m_implementation_type;

private:
	void connect_events();

	std::unique_ptr<sf::RenderWindow> m_window;
	std::unique_ptr<sf::Texture> m_texture;
	std::unique_ptr<sf::Sprite> m_sprite;

	std::unique_ptr<uint8_t[]> m_raw_buffer;

	std::unique_ptr<Buffer> m_buffer;
	std::unique_ptr<Viewport> m_viewport;
	std::unique_ptr<EventRouter> m_eventrouter;
	std::unique_ptr<Impl> m_implementation;
};
}

#endif /* SRC_APPLICATION_HPP_ */