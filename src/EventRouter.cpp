#include "EventRouter.hpp"
#include "Viewport.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System/Vector2.hpp>

namespace mandelbrot {

// TODO: eliminate viewport dependency by externalizing event handling to other objects
EventRouter::EventRouter(sf::Window& window) : m_window(window) {
}

void EventRouter::Poll() {
	sf::Event event;
	while (m_window.pollEvent(event)) {
		switch (event.type) {
		case sf::Event::Closed: {
			m_close_event(event);
			break;
		}
		case sf::Event::Resized: {
			m_resize_event(event);
			break;
		}
		case sf::Event::MouseWheelScrolled: {
			m_mouse_wheel_event(event);
			break;
		}
		case sf::Event::KeyReleased: {
			m_key_released_event(event);
			break;
		}
		default:
			break;
		}
	}
}
} /* namespace mandelbrot */
