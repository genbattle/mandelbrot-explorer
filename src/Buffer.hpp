#pragma once
#ifndef SRC_BUFFER_HPP_
#define SRC_BUFFER_HPP_
#include <cstdint>

namespace mandelbrot {

// Encapsulate raw frame buffer data
struct Buffer {
	// For some reason make_unique requires a user-defined constructor
	Buffer(uint8_t* data_, int width_, int height_, int bpp_)
	    : data(data_), width(width_), height(height_), bpp(bpp_) {}

	uint8_t* data;          // pointer to raw pixel data
	int width, height, bpp; // image dimensions
};

} /* namespace mandelbrot */

#endif /* SRC_BUFFER_HPP_ */
