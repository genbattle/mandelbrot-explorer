#pragma once
#ifndef SRC_VIEWPORT_HPP_
#define SRC_VIEWPORT_HPP_
#include <cstdint>

namespace mandelbrot {

// Group viewport properties
struct Viewport {
	// For some reason make_unique requires a user-defined constructor
	Viewport(float left_, float top_, float width_, float height_, uint32_t limit_)
	    : left(left_), top(top_), width(width_), height(height_), limit(limit_) {}
	float left, top, width, height;
	uint32_t limit;
};

} /* namespace mandelbrot */

#endif /* SRC_VIEWPORT_HPP_ */
